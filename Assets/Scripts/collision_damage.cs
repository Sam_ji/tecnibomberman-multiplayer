﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class collision_damage : MonoBehaviour {
	public int damageAmount;

	public void OnCollisionEnter2D(Collision2D col){
		var hit = col.gameObject;
		var hitPlayer = hit.GetComponent<player_combat>();
		if (hitPlayer != null){
			hit.GetComponent<player_combat>().TakeDamage(damageAmount);
		}
	}


}

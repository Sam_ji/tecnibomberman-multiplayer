﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class player_combat : NetworkBehaviour {

	public const int maxHealth = 100;
	public GameObject dead_canvas;

	[SyncVar]
	public int health = maxHealth;

	public void TakeDamage(int amount){
		if (!isServer)
			return;

		health -= amount;
		if (health <= 0){
			health = maxHealth;
			RpcRespawn();
		}
	}

	[Client]
	void ShowDeadGui(){
		if (isLocalPlayer && isClient){
			var dead_canvas_i = (GameObject)Instantiate(dead_canvas,transform.position,transform.rotation);
			Destroy(dead_canvas_i,5.0f);
		}
	}

	[ClientRpc]
	void RpcRespawn(){
		
			gameObject.GetComponent<player_controller>().enabled = false;
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			gameObject.GetComponent<BoxCollider2D>().enabled = false;
			Invoke("RpcRespawnEnd",5.0f);
	
		ShowDeadGui();
	}
		
	[ClientRpc]
	void RpcRespawnEnd(){
		
			gameObject.GetComponent<player_controller>().enabled = true;
			gameObject.GetComponent<SpriteRenderer>().enabled = true;
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
			transform.position = Vector3.zero;

	}

}

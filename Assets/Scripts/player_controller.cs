﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class player_controller : NetworkBehaviour {

	public Camera camera_prefab;
	public GameObject shit_bomb_prefab;
	[SyncVar]
	public double last_bomb_timestamp = 0.0f;


	//This is invoked everytime this script is started as a local player
	public override void OnStartLocalPlayer()
	{
		//We create a local camera to follow the player
		Camera player_camera = (Camera)Instantiate(camera_prefab,transform.position,transform.rotation);
		player_camera.transform.SetParent(transform);
		player_camera.transform.localPosition = new Vector3(0.0f,0.0f,-10.0f);

		//We change the color of the player's sprite
		GetComponent<SpriteRenderer>().color = Color.white;
	}

	// Update is called once per frame
	void Update () {
		if (!this.isLocalPlayer)
			return;
		
		float x = Input.GetAxis("Horizontal")*0.1f;
		float y = Input.GetAxis("Vertical")*0.1f;
		CmdMove (x, y);



		if(Input.GetButtonDown("Jump") && Network.time >last_bomb_timestamp){
			//Activate the cooldown
			last_bomb_timestamp = Network.time + shit_bomb_prefab.GetComponent<shit_bomb_script>().time_2_explode;
			//Send request for spawning to the server
			CmdPlantShitBomb();
		}
	}

	[Command]
	void CmdPlantShitBomb(){
		var shit_bomb = (GameObject)Instantiate(shit_bomb_prefab,transform.position,transform.rotation);
		NetworkServer.Spawn(shit_bomb);
		Destroy(shit_bomb,shit_bomb.GetComponent<shit_bomb_script>().time_2_explode);
	}

	[Command]
	void CmdMove(float x, float y){
		RpcMove( x,  y);
	}
	[ClientRpc]
	void RpcMove(float x, float y){
		if (!this.isLocalPlayer)
			return;

		if (Mathf.Abs (x) >= Mathf.Abs (y) && x > 0) {
			GetComponent<Animator> ().SetInteger ("orientation", 2);
			GetComponent<SpriteRenderer> ().flipX = true;
		}
		if (Mathf.Abs (x) >= Mathf.Abs (y) && x < 0) {
			GetComponent<Animator> ().SetInteger ("orientation", 4);
			GetComponent<SpriteRenderer> ().flipX = false;
		}

		if (Mathf.Abs (x) < Mathf.Abs (y) && y > 0) {
			GetComponent<Animator> ().SetInteger ("orientation", 1);
		}
		if (Mathf.Abs (x) < Mathf.Abs (y) && y < 0) {
			GetComponent<Animator> ().SetInteger ("orientation", 3);
		}

		if(x == 0.0f && y == 0.0f)
			GetComponent<Animator> ().SetInteger ("orientation", -1);

		transform.Translate(x, y, 0);
		
	}


}

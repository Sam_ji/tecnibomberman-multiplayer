﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger_damage : MonoBehaviour {
	public int damageAmount; 
	public void OnTriggerEnter2D(Collider2D col){
		var hit = col.gameObject;
		var hitPlayer = hit.GetComponent<player_combat>();
		if (hitPlayer != null){
			hit.GetComponent<player_combat>().TakeDamage(damageAmount);
		}

	}
}
